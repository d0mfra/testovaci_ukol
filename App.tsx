/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

function App() {
  const [lowestFirst, setLowestFirst] = useState(true);
  const [array, setArray] = useState<
    {
      title: string;
      episode_number: number;
      main_characters: string;
      description: string;
      poster: string;
      hero_image: string;
    }[]
  >([]);
  useEffect(() => {
    fetch(
      'https://raw.githubusercontent.com/RyanHemrick/star_wars_movie_app/master/movies.json',
    ).then(response => {
      response.json().then(data => {
        setArray(data.movies);
      });
    });
  }, []);
  const footer = () => {
    return (
      <View>
        <TouchableOpacity
          style={styles.buttonContainer}
          onPress={() => {
            const sortArray = [...array];
            setArray(
              sortArray.sort((a, b) => {
                const sortOrder = b.episode_number - a.episode_number;
                if (lowestFirst) {
                  return sortOrder;
                } else {
                  return sortOrder * -1;
                }
              }),
            );
            setLowestFirst(!lowestFirst);
          }}>
          <Text style={styles.buttonText}>sort</Text>
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <View>
      <FlatList
        ListFooterComponent={footer}
        numColumns={2}
        data={array}
        renderItem={({item}) => (
          <View style={styles.imgContainer} key={item.episode_number}>
            <Image
              style={styles.img}
              source={{
                uri:
                  'https://raw.githubusercontent.com/RyanHemrick/star_wars_movie_app/master/public/images/' +
                  item.poster,
              }}
            />
            <Text style={styles.text}>
              {item.title + ' ' + item.episode_number}
            </Text>
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    width: 125,
    height: 150,
    resizeMode: 'contain',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  buttonText: {
    fontSize: 20,
    textTransform: 'uppercase',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgContainer: {
    height: 250,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 130,
  },
});

export default App;
